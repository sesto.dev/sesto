<?php
/* =============================================================================
 * Naranza Sesto - Copyright (c) Andrea Davanzo - License MPL v2.0 - sesto.dev
 * ========================================================================== */

declare(strict_types = 1);

const SESTO_DIR = __DIR__;
const SESTO_RELEASE = '2020.06';
const SESTO_CODENAME = 'Mantegna';
