<?php
/* =============================================================================
 * Naranza Sesto - Copyright (c) Andrea Davanzo - License MPL v2.0 - sesto.dev
 * ========================================================================== */

declare(strict_types = 1);

function sesto_hook_attach(array &$hooks, string $name, callable $callback, int $priority = 50): void
{
  $hooks[$name][$priority][] = $callback;
}
